using System;

class exercise7
{
   static void Main()
   {
      int n = int.Parse(Console.ReadLine());
      int x = 0;

      for (int i = 0; i < n; i++)
      {
         x = int.Parse(Console.ReadLine());

         if (x == 0) {
            Console.WriteLine("NULL");
         }

         else if (x % 2 == 1 && x > 0) {
            Console.WriteLine($"ODD POSITIVE");
         }

         else if (Math.Abs(x) % 2 == 1 && x < 0) {
            Console.WriteLine($"ODD NEGATIVE");
         }

         else if (x % 2 == 0 && x > 0) {
            Console.WriteLine("EVEN POSITIVE");
         }

         else if (x % 2 == 0 && x < 0) {
            Console.WriteLine("EVEN NEGATIVE");
         }
      }
   }
}
