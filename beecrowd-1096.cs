using System;

class exercise7
{
   static void Main()
   {
      int i = 1;
      int j = 7;

      while (i <= 9)
      {
         Console.WriteLine($"I={i} J={j}");

         j--;

         if (j == 4) {
            i += 2;
            j = 7;
         }
      }
   }
}
