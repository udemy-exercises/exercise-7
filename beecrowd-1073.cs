using System;

class exercise7
{
   static void Main()
   {
      int n = int.Parse(Console.ReadLine());

      if (n < 5)
      {
         return;
      }

      else
      {
         for (int i = 2; i <= n; i += 2) {
            Console.WriteLine($"{i}^2 = {Math.Pow(i, 2)}");
         }
      }
   }
}
