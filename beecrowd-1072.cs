using System;

class exercise7
{
   static void Main()
   {
      int n = int.Parse(Console.ReadLine());
      int x = 0;
      int ins = 0;
      int outs = 0;

      for (int i = 0; i < n; i++)
      {
         x = int.Parse(Console.ReadLine());

         if (x >= 10 && x <= 20) {
            ins++;
         }
         
         else {
            outs++;
         }
      }

      Console.WriteLine($"{ins} in");
      Console.WriteLine($"{outs} out");
   }
}
