using System;

class exercise7
{
   static void Main()
   {
      int x = 0;
      int y = 0;
      int pos = 0;

      for (int i = 1; i < 101; i++)
      {
         x = int.Parse(Console.ReadLine());

         if (x > y) {
            y = x;
            pos = i;
         }
      }

      Console.WriteLine(y);
      Console.WriteLine(pos);
   }
}
