using System;

class exercise7
{
   static void Main()
   {
      int i = 1;
      int j = 7;

      for (int count = 1; i <= 9; count++, j--)
      {
         Console.WriteLine($"I={i} J={j}");

         if (count == 3) {
            j += 5;
            i += 2;
            count = 0;
         }
      }
   }
}
